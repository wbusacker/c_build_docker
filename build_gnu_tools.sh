#!/bin/sh

# There are a number of programs assumed to already be installed on the system to build
# make
# wget
# tar
# zstd
# gcc
# glibc

# Configure the versions of the GNU Toolchain
gcc_version=11.2.0
gdb_version=11.1
binutils_version=2.37
gmp_version=6.2.1
m4_version=1.4.19
mpfr_version=4.1.0
mpc_version=1.2.1

# Prefix definitions
pkg_author='Will Busacker'
prefix=

# Installation directory
install_dir=/opt/

# Build Cores
num_cores=12

# Create all needed subdirectories
cd $install_dir

for product in gcc gdb binutils m4 gmp mpfr mpc
do 
    mkdir $product
    mkdir $product/build
done

###############################################################################
#                                                                             #
#                              Product Downloads                              #
#                                                                             #
#                  Always download all products back to back                  #
#                                                                             #
###############################################################################

cd $install_dir/gcc
wget ftp.gnu.org/gnu/gcc/gcc-$gcc_version/gcc-$gcc_version.tar.gz

cd $install_dir/gdb
wget ftp.gnu.org/gnu/gdb/gdb-$gdb_version.tar.gz

cd $install_dir/binutils
wget ftp.gnu.org/gnu/binutils/binutils-$binutils_version.tar.gz

cd $install_dir/gmp
wget ftp.gnu.org/gnu/gmp/gmp-$gmp_version.tar.zst

cd $install_dir/m4
wget ftp.gnu.org/gnu/m4/m4-$m4_version.tar.gz

cd $install_dir/mpfr
wget ftp.gnu.org/gnu/mpfr/mpfr-$mpfr_version.tar.gz

cd $install_dir/mpc
wget ftp.gnu.org/gnu/mpc/mpc-$mpc_version.tar.gz

###############################################################################
#                                                                             #
#                                 GNU Debugger                                #
#                                                                             #
###############################################################################
cd $install_dir/gdb
tar -xzvf gdb-$gdb_version.tar.gz

cd build
$install_dir/gdb/$gdb_version/configure --prefix=$install_dir/gdb/$gdb_version --program-prefix=$prefix --with-pkgversion="$pkg_author"

make -j $num_cores
make install

# Clean up GDB Extras
rm -rf $install_dir/gdb/gdb-$gdb_version/ $install_dir/gdb/gdb-$gdb_version.tar.gz $install_dir/gdb/build/

###############################################################################
#                                                                             #
#                                      m4                                     #
#                                                                             #
###############################################################################
cd $install_dir/m4
tar -xzvf m4-$m4_version.tar.gz

cd build
$install_dir/m4/m4-$m4_version/configure 

make -j $num_cores
make install

cd ..
rm -rf build/ m4-$m4_version/ m4-$m4_version.tar.gz

###############################################################################
#                                                                             #
#                        GNU Multiple Precision Library                       #
#                                                                             #
###############################################################################
cd $install_dir/gmp
tar -I zstd -xvf gmp-$gmp_version.tar.zst 

cd build
$install_dir/gmp/gmp-$gmp_version/configure
make -j $num_cores
make install

cd ..
rm -rf build/ gmp-$gmp_version/ gmp-$gmp_version.tar.zst

###############################################################################
#                                                                             #
#                  Multiple Precision Floating-Point Library                  #
#                                                                             #
###############################################################################
cd $install_dir/mpfr
tar -xzvf mpfr-$mpfr_version.tar.gz

cd build
$install_dir/mpfr/mpfr-$mpfr_version/configure
make -j $num_cores
make install

cd ..
rm -rf build/ mpfr-$mpfr_version/ mpfr-$mpfr_version.tar.gz

###############################################################################
#                                                                             #
#                      Multiple Precision Complex Library                     #
#                                                                             #
###############################################################################

cd $install_dir/mpc
tar -xzvf mpc-$mpc_version.tar.gz

cd build
$install_dir/mpc/mpc-$mpc_version/configure
make -j $num_cores
make install

cd ..
rm -rf build/ mpc-$mpc_version/ mpc-$mpc_version.tar.gz

###############################################################################
#                                                                             #
#                           GNU Compiler Collection                           #
#                                                                             #
###############################################################################

# Build GCC
cd $install_dir/gcc
tar -xzvf gcc-$gcc_version.tar.gz

cd build
$install_dir/gcc/gcc-$gcc_version/configure --prefix=$install_dir/gcc/ --program-prefix=$prefix --with-pkgversion="$pkg_author" --disable-multilib --enable-languages=c,c++
make -j $num_cores
make install

cd ..
rm -rf build/ gcc--$gcc_version/ gcc-$gcc_version.tar.gz