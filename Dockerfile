FROM debian

ENV DEBIAN_FRONTEND noninteractive

WORKDIR /app

COPY . . 

RUN apt-get update && \
    apt-get -y install make wget tar zstd gcc g++ file gdb
RUN chmod +x ./build_gnu_tools.sh
RUN ./build_gnu_tools.sh