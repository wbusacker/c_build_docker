
BASE_PACKAGE_DIR=/opt

M4_VERSION=1.4.19
GMP_VERSION=6.2.1
MPFR_VERSION=4.2.0
MPC_VERSION=1.3.1
GCC_VERSION=12.2.0
GDB_VERSION=12.1

PREFIX=
PKG_AUTHOR='Will Busacker'

SOURCE_SERVER=ftp.gnu.org/gnu/

NUM_CPU_CORES=12

fetch_debian_prereq(){
    apt-get install -y make wget tar zstd gcc g++ libc6-dev texinfo expat
}

 #--with-pkgversion="$PKG_AUTHOR"

build_m4(){
    cd $BASE_PACKAGE_DIR
    mkdir -p m4
    cd m4
    wget $SOURCE_SERVER/m4/m4-$M4_VERSION.tar.gz 
    tar -xzvf m4-$M4_VERSION.tar.gz
    mkdir -p build
    cd build
    $BASE_PACKAGE_DIR/m4/m4-$M4_VERSION/configure -prefix=$BASE_PACKAGE_DIR/m4/$M4_VERSION --program-prefix=$PREFIX
    make -j$NUM_CPU_CORES
    make install
    cd ..
    rm -rf build m4-$M4_VERSION.tar.gz m4-$M4_VERSION
    PATH=$BASE_PACKAGE_DIR/m4/$M4_VERSION/bin/:$PATH
}

build_gmp(){
    cd $BASE_PACKAGE_DIR
    mkdir -p gmp
    cd gmp
    wget $SOURCE_SERVER/gmp/gmp-$GMP_VERSION.tar.xz
    tar -xvf gmp-$GMP_VERSION.tar.xz
    mkdir -p build
    cd build
    $BASE_PACKAGE_DIR/gmp/gmp-$GMP_VERSION/configure -prefix=$BASE_PACKAGE_DIR/gmp/$GMP_VERSION --program-prefix=$PREFIX 
    make -j$NUM_CPU_CORES
    make install
    cd ..
    rm -rf build gmp-$GMP_VERSION.tar.xz gmp-$GMP_VERSION

}

build_mpfr(){
    mkdir -p $BASE_PACKAGE_DIR/mpfr
    cd $BASE_PACKAGE_DIR/mpfr
    wget $SOURCE_SERVER/mpfr/mpfr-$MPFR_VERSION.tar.gz
    tar -xzvf mpfr-$MPFR_VERSION.tar.gz
    mkdir -p build
    cd build
    $BASE_PACKAGE_DIR/mpfr/mpfr-$MPFR_VERSION/configure -prefix=$BASE_PACKAGE_DIR/mpfr/$MPFR_VERSION --program-prefix=$PREFIX --with-gmp=$BASE_PACKAGE_DIR/gmp/$GMP_VERSION
    make -j$NUM_CPU_CORES
    make install
    cd ..
    rm -rf build mpfr-$MPFR_VERSION.tar.gz mpfr-$MPFR_VERSION
}

build_mpc(){
    mkdir -p $BASE_PACKAGE_DIR/mpc
    cd $BASE_PACKAGE_DIR/mpc
    wget $SOURCE_SERVER/mpc/mpc-$MPC_VERSION.tar.gz
    tar -xzvf mpc-$MPC_VERSION.tar.gz
    mkdir -p build
    cd build
    $BASE_PACKAGE_DIR/mpc/mpc-$MPC_VERSION/configure -prefix=$BASE_PACKAGE_DIR/mpc/$MPC_VERSION --program-prefix=$PREFIX --with-gmp=$BASE_PACKAGE_DIR/gmp/$GMP_VERSION --with-mpfr=$BASE_PACKAGE_DIR/mpfr/$MPFR_VERSION
    make -j$NUM_CPU_CORES
    make install
    cd ..
    rm -rf build mpc-$MPC_VERSION.tar.gz mpc-$MPC_VERSION
}

build_gcc(){
    mkdir -p $BASE_PACKAGE_DIR/gcc
    cd $BASE_PACKAGE_DIR/gcc
    wget $SOURCE_SERVER/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz
    tar -xzvf gcc-$GCC_VERSION.tar.gz
    mkdir -p build
    cd build
    $BASE_PACKAGE_DIR/gcc/gcc-$GCC_VERSION/configure -prefix=$BASE_PACKAGE_DIR/gcc/$GCC_VERSION --program-prefix=$PREFIX --with-pkgversion="$PKG_AUTHOR" --with-gmp=$BASE_PACKAGE_DIR/gmp/$GMP_VERSION --with-mpfr=$BASE_PACKAGE_DIR/mpfr/$MPFR_VERSION --with-mpc=$BASE_PACKAGE_DIR/mpc/$MPC_VERSION --disable-multilib --enable-languages=c,c++
    make -j$NUM_CPU_CORES
    make install
    cd ..
    rm -rf build gcc-$GCC_VERSION.tar.gz gcc-$GCC_VERSION
}

build_gdb(){
    mkdir -p $BASE_PACKAGE_DIR/gdb
    cd $BASE_PACKAGE_DIR/gdb
    wget $SOURCE_SERVER/gdb/gdb-$GDB_VERSION.tar.gz
    tar -xzvf gdb-$GDB_VERSION.tar.gz
    mkdir -p build
    cd build
    $BASE_PACKAGE_DIR/gdb/gdb-$GDB_VERSION/configure -prefix=$BASE_PACKAGE_DIR/gdb/$GDB_VERSION --program-prefix=$PREFIX --with-pkgversion="$PKG_AUTHOR" --with-libgmp-prefix=$BASE_PACKAGE_DIR/gmp/$GMP_VERSION/ --with-mpfr=$BASE_PACKAGE_DIR/mpfr/$MPFR_VERSION/ --with-mpc=$BASE_PACKAGE_DIR/mpc/$MPC_VERSION/
    make
    make install
    cd ..
    rm -rf build gdb-$GDB_VERSION.tar.gz gdb-$GDB_VERSION
}




# Must be built in this order

build_m4
build_gmp
build_mpfr
build_mpc
build_gcc
